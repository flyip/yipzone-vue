FROM nginx:1.15.2-alpine
COPY ./dist /var/www
COPY nginx.conf /etc/nginx/nginx.conf
COPY ./certs/www.yipzone.com.key /etc/nginx/certs/www.yipzone.com.key
COPY ./certs/www.yipzone.com.crt /etc/nginx/certs/www.yipzone.com.crt
EXPOSE 80/tcp 443/tcp
ENTRYPOINT ["nginx","-g","daemon off;"]
