export default {
  jwt: state => state.token,
  tokenData: (state, getters) =>
    state.token ? JSON.parse(atob(getters.jwt.split(".")[1])) : null,
  tokenAdmin: (state, getters) =>
    getters.tokenData ? getters.tokenData.admin : null,
  tokenName: (state, getters) =>
    getters.tokenData ? getters.tokenData.name : null
};
