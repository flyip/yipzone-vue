import auth from "../services/authService";
import router from "../router";

export default {
  doLogin({ commit }, { email, password }) {
    commit("LOGIN_START");

    auth
      .login(email, password)
      .then(response => {
        const token = response;
        commit("LOGIN_STOP", null);
        commit("UPDATE_ACCESS_TOKEN", token);
        commit("UPDATE_LOGGED_IN", true);

        router.push("/video-list");
      })
      .catch(error => {
        commit("LOGIN_STOP", error.response.data.error);
        commit("UPDATE_ACCESS_TOKEN", null);
        commit("UPDATE_LOGGED_IN", false);
        sessionStorage.removeItem("accessToken");
      });
  },
  fetchAccessToken({ commit }) {
    commit("UPDATE_ACCESS_TOKEN", sessionStorage.getItem("accessToken"));
  },
  logout({ commit }) {
    return new Promise(resolve => {
      commit("LOGOUT");
      sessionStorage.removeItem("accessToken");
      // delete axios.defaults.headers.common["Authorization"];
      resolve();
    });
  }
};
