export default {
  token: null,
  loggingIn: false,
  loginError: null,
  loggedIn: false
};
