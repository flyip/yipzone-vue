export default {
  LOGIN_START: state => (state.loggingIn = true),
  LOGIN_STOP: (state, errorMessage) => {
    state.loggingIn = false;
    state.loginError = errorMessage;
  },
  UPDATE_LOGGED_IN: (state, loginState) => {
    state.loggedIn = loginState;
  },
  UPDATE_ACCESS_TOKEN: (state, token) => {
    state.token = token;
  },
  LOGOUT: state => {
    state.token = null;
    state.loggedIn = false;
  }
};
