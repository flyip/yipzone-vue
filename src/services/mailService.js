import axios from "axios";

const apiEndpoint = process.env.VUE_APP_ENDPOINT + "/mail/send";

export function sendEmail(data) {
  return axios.post(apiEndpoint, data);
}
