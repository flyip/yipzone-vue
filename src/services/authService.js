import jwtDecode from "vue-jwt-decode";
import axios from "axios";

const apiEndpoint = process.env.VUE_APP_ENDPOINT + "/auth";
const tokenKey = "accessToken";

const jwt = getJwt();
axios.defaults.headers.common["x-auth-token"] = jwt;
axios.defaults.headers.post["x-auth-token"] = jwt;
axios.defaults.headers.put["x-auth-token"] = jwt;

export async function login(email, password) {
  const { data: jwt } = await axios.post(apiEndpoint, { email, password });
  sessionStorage.setItem(tokenKey, jwt);

  return jwt;
}

export function loginWithJwt(jwt) {
  sessionStorage.setItem(tokenKey, jwt);
}

export function logout() {
  sessionStorage.removeItem(tokenKey);
}

export function getCurrentUser() {
  try {
    const jwt = sessionStorage.getItem(tokenKey);
    return jwtDecode(jwt);
  } catch (ex) {
    return null;
  }
}

export function getJwt() {
  return sessionStorage.getItem(tokenKey);
}

export default {
  login,
  loginWithJwt,
  logout,
  getCurrentUser,
  getJwt
};
