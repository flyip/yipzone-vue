// import httpService from "./httpService";
import axios from "axios";
import { getJwt } from "./authService";

const apiEndpoint = process.env.VUE_APP_ENDPOINT + "/videos";
// const apiEndpoint = "https://localhost:3001/api/videos";
const videos = [
  {
    path: "life",
    title: "Life in General"
  },
  {
    path: "life/australia",
    title: "Life in General - Australia"
  },
  {
    path: "life/world",
    title: "Life in General - World"
  },
  {
    path: "life/tanks",
    title: "Life in General - Tanks"
  },
  {
    path: "life/3d",
    title: "Life in General - 3D Printing"
  },
  {
    path: "life/reviews",
    title: "Life in General - Reviews"
  },
  {
    path: "mods",
    title: "Modding"
  },
  {
    path: "mods/general",
    title: "Modding - General"
  },
  {
    path: "mods/wheel",
    title: "Modding - Steering Wheels"
  },
  {
    path: "mods/shifter",
    title: "Modding - Shifter"
  },
  {
    path: "gaming",
    title: "Gaming"
  },
  {
    path: "gaming/assetto-corsa",
    title: "Gaming - Assetto Corsa"
  },
  {
    path: "gaming/dirt",
    title: "Gaming - Dirt Series"
  },
  {
    path: "gaming/project-cars",
    title: "Gaming - Project CARS"
  },
  {
    path: "gaming/rbr",
    title: "Gaming - Richard Burns Rally"
  },
  {
    path: "gaming/rfactor",
    title: "Gaming - rFactor"
  },
  {
    path: "gaming/wrc",
    title: "Gaming - WRC Series"
  }
];

export function getVideoTitle(cat, subcat) {
  const path = subcat ? cat + "/" + subcat : cat;

  let catSub = videos.find(x => x.path === path);
  if (catSub) {
    return catSub.title;
  } else {
    return false;
  }
}

function videoUrl(id) {
  return `${apiEndpoint}/${id}`;
}

export function getVideos() {
  return axios.get(apiEndpoint).then(response => response.data);
}

export function getVideosInCategory(category) {
  return axios
    .get(apiEndpoint + "/cat/" + category)
    .then(response => response.data);
}

export function getVideosInCategorySubCategory(category, subcategory) {
  return axios
    .get(apiEndpoint + "/cat/" + category + "/" + subcategory)
    .then(response => response.data);
}

export function getVideo(videoId) {
  return axios.get(videoUrl(videoId)).then(response => response.data);
}

export function saveVideo(video) {
  if (video._id) {
    const body = { ...video };
    delete body._id;
    if (!body.active) {
      body.active = true;
    }
    return axios.put(videoUrl(video._id), body, {
      headers: {
        "Content-Type": "application/json; charset=utf-8",
        "x-auth-token": getJwt()
      }
    });
  }

  const body = { ...video };
  delete body._id;
  return axios
    .post(apiEndpoint, JSON.stringify(body), {
      headers: {
        "Content-Type": "application/json; charset=utf-8",
        "x-auth-token": getJwt()
      }
    })
    .then(response => response.data)
    .catch(error => {
      console.log(error);
    });
}

export function deleteVideo(videoId) {
  return axios
    .delete(videoUrl(videoId), {
      headers: {
        "Content-Type": "application/json; charset=utf-8",
        "x-auth-token": getJwt()
      }
    })
    .then(response => response.data)
    .catch(error => {
      console.log(error);
    });
}

export function getSubCategoryName(path) {
  const subCats = [
    { subPath: "/life/australia", subCat: "Australia" },
    { subPath: "/life/world", subCat: "World" },
    { subPath: "/life/tanks", subCat: "Tanks" },
    { subPath: "/life/3d", subCat: "3D Printing" },
    { subPath: "/life/reviews", subCat: "Reviews" },
    { subPath: "/mods/general", subCat: "General" },
    { subPath: "/mods/wheel", subCat: "Steering Wheels" },
    { subPath: "/mods/shifter", subCat: "Shifter" },
    { subPath: "/gaming/assetto-corsa", subCat: "Assetto Corsa" },
    { subPath: "/gaming/dirt", subCat: "Dirt Series" },
    { subPath: "/gaming/project-cars", subCat: "Project CARS" },
    { subPath: "/gaming/rbr", subCat: "Richard Burns Rally" },
    { subPath: "/gaming/rfactor", subCat: "rFactor" },
    { subPath: "/gaming/wrc", subCat: "WRC Series" },
    { subPath: "/gaming/other", subCat: "Miscelleneous" }
  ];
  const subCategory = subCats.find(function(subCategory) {
    return subCategory.subPath === path;
  });
  return subCategory ? " - " + subCategory.subCat : "";
}
