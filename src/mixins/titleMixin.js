function getTitle(vm) {
  const { docTitle } = vm.$options;
  if (docTitle) {
    return typeof docTitle === "function" ? docTitle.call(vm) : docTitle;
  }
}

export default {
  created() {
    const title = getTitle(this);
    if (title) {
      document.title = title;
    }
  }
};
