import Vue from "vue";
import VueLazyLoad from "vue-lazyload";
import BootstrapVue from "bootstrap-vue";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import "./css/style.css";
import "./css/video.css";

import App from "./App.vue";
import router from "./router";
import store from "./store/";
import titleMixin from "./mixins/titleMixin";

require("vue-image-lightbox/dist/vue-image-lightbox.min.css");

Vue.use(BootstrapVue);
Vue.use(VueLazyLoad);
Vue.mixin(titleMixin);

Vue.config.productionTip = false;

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount("#app");
