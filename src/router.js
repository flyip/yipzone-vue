import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home";
import Login from "./views/Login";
import Pets from "./views/Pets";
import Videos from "./views/Videos";
import Tme from "./views/Tme";
import Dc5 from "./views/Dc5";
import Contact from "./views/Contact";
import NotFound from "./views/NotFound";
import VideoList from "./views/admin/VideoList";
import VideoForm from "./views/admin/VideoForm";

import { getVideoTitle } from "./services/videoService";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/tme",
      name: "tme",
      component: Tme
    },
    {
      path: "/dc5",
      name: "dc5",
      component: Dc5
    },
    {
      path: "/pets",
      name: "pets",
      component: Pets
    },
    {
      path: "/contact",
      name: "contact",
      component: Contact
    },
    {
      path: "/login",
      name: "login",
      component: Login
    },
    {
      path: "/video-list",
      name: "videoList",
      component: VideoList
    },
    {
      path: "/video/:videoId?",
      name: "videoForm",
      component: VideoForm,
      props: true
    },
    {
      path: "/:cat/:subcat?",
      name: "videos",
      component: Videos,
      props: true,
      beforeEnter: (to, from, next) => {
        if (getVideoTitle(to.params.cat, to.params.subcat)) {
          next();
        } else {
          next({ path: "/404" });
        }
      }
    },
    {
      path: "/404",
      name: "404",
      component: NotFound
    }
  ]
});
